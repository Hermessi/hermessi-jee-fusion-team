package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;

@Local
public interface CategoryServicesLocal {

	Boolean addCategory(Category theCategory);

	Long addCategoryAndReturnId(Category theCategory);

	Category findCategoryById(Long theId);

	Boolean updateCategory(Category theCategory);

	Boolean deleteCategory(Category theCategory);

	Boolean deleteCategoryById(Long theId);

	List<Category> findAllCategory();

}
