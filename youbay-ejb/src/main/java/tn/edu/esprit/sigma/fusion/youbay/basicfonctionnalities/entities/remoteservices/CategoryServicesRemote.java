package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;

@Remote
public interface CategoryServicesRemote {

	Boolean addCategory(Category theCategory);

	Long addCategoryAndReturnId(Category theCategory);

	Category findCategoryById(Long theId);

	Boolean updateCategory(Category theCategory);

	Boolean deleteCategory(Category theCategory);

	Boolean deleteCategoryById(Long theId);

	List<Category> findAllCategory();

}
