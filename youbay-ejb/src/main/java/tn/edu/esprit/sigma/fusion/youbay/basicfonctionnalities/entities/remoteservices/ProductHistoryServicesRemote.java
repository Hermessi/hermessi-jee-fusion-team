package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices;

import java.util.List;

import javax.ejb.Remote;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;

@Remote
public interface ProductHistoryServicesRemote {

	Boolean addProductHistory(ProductHistory theProductHistory);

	Long addProductHistoryAndReturnId(ProductHistory theProductHistory);

	ProductHistory findProductHistoryById(Long theId);

	Boolean updateProductHistory(ProductHistory theProductHistory);

	Boolean deleteProductHistory(ProductHistory theProductHistory);

	Boolean deleteProductHistoryById(Long theId);

	List<ProductHistory> findAllProductHistory();

}
