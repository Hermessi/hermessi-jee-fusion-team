package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;

@Local
public interface ProductHistoryServicesLocal {

	Boolean addProductHistory(ProductHistory theProductHistory);

	Long addProductHistoryAndReturnId(ProductHistory theProductHistory);

	ProductHistory findProductHistoryById(Long theId);

	Boolean updateProductHistory(ProductHistory theProductHistory);

	Boolean deleteProductHistory(ProductHistory theProductHistory);

	Boolean deleteProductHistoryById(Long theId);

	List<ProductHistory> findAllProductHistory();
}
