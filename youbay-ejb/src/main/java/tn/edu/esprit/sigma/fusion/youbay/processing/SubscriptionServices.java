/*
 * 
 * Author Marwen
 */
package tn.edu.esprit.sigma.fusion.youbay.processing;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.BuyerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation.BuyerServices;
import tn.edu.esprit.sigma.fusion.youbay.utilities.MessagingServicesRemote;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class SubscriptionServices
 */
@Stateless
public class SubscriptionServices implements SubscriptionServicesRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@EJB
	SellerServicesLocal sellerServicesLocal;
	
	@EJB
	BuyerServicesLocal buyerServicesLocal;
	
	@EJB
	MessagingServicesRemote messagingServicesRemote;
	
	@EJB
	EncryptionServicesRemote encryptionServicesRemote;
	
    /**
     * Default constructor. 
     */
    public SubscriptionServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void subscribeSeller(Seller seller) {
		try {
			String tokenString = ""+seller.getYouBayUserId()+seller.getFirstName()+seller.getLastName();
			sellerServicesLocal.addSeller(seller);
			encryptionServicesRemote.encryptString(tokenString);
			messagingServicesRemote.sendEmail(
					seller.getEmail(),
					"Youbay subscription confirmation",
					"Thank you for subscribing to our services.  "
					+ "Please provide this token at login page :"+
					encryptionServicesRemote.encryptString(tokenString)
					);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

	@Override
	public void subscribeBuyer(Buyer buyer) {
		try {
			String tokenString = ""+buyer.getYouBayUserId()+buyer.getFirstName()+buyer.getLastName();
			buyerServicesLocal.addBuyer(buyer);
			encryptionServicesRemote.encryptString(tokenString);
			messagingServicesRemote.sendEmail(
					buyer.getEmail(),
					"Youbay subscription confirmation",
					"Thank you for subscribing to our services.  "

					
					);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}


}
