package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ProductServicesRemote;

/**
 * Session Bean implementation class ProductServices
 */
@Stateless
public class ProductServices implements ProductServicesRemote,
		ProductServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public ProductServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addProduct(Product theProduct) {
		Boolean b = false;
		try {
			entityManager.persist(theProduct);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Product findProductById(Long theId) {
		return entityManager.find(Product.class, theId);
	}

	@Override
	public Boolean updateProduct(Product theProduct) {
		boolean b = false;
		try {
			entityManager.merge(theProduct);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteProduct(Product theProduct) {
		Boolean b = false;
		try {
			theProduct = findProductById(theProduct.getProductId());
			entityManager.remove(theProduct);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteProductById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findProductById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> findAllProduct() {
		Query query = entityManager.createQuery("select e from Product e ");
		return query.getResultList();
	}

	@Override
	public Long addProductAndReturnId(Product theProduct) {

		try {
			entityManager.persist(theProduct);
			entityManager.flush();
			return theProduct.getProductId();
		} catch (Exception e) {
		}
		return null;	}

}
