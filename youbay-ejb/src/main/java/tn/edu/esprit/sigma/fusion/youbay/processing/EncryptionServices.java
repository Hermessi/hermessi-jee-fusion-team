/*
 * 
 * Author Marwen
 */

package tn.edu.esprit.sigma.fusion.youbay.processing;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class EncryptionServices
 */
@Stateless
@LocalBean
public class EncryptionServices implements EncryptionServicesRemote {

    /**
     * Default constructor. 
     */
	
	
    public EncryptionServices() {

    }

	@Override
	public String encryptString(String string) {
	    String sha1 = "";
	    try
	    {
	        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
	        crypt.reset();
	        crypt.update(string.getBytes("UTF-8"));
	        sha1 = byteToHex(crypt.digest());
	    }
	    catch(NoSuchAlgorithmException e)
	    {
	        e.printStackTrace();
	    }
	    catch(UnsupportedEncodingException e)
	    {
	        e.printStackTrace();
	    }
	    return sha1;
	}

	@Override
	public String byteToHex(byte[] hash) {
		Formatter formatter = new Formatter();
	    for (byte b : hash)
	    {
	        formatter.format("%02x", b);
	    }
	    String result = formatter.toString();
	    formatter.close();
	    return result;
	}


}
