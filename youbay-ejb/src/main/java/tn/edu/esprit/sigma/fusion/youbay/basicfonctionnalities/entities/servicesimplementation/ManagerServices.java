package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.servicesimplementation;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ManagerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.ManagerServicesRemote;

/**
 * Session Bean implementation class ManagerServices
 */
@Stateless
public class ManagerServices implements ManagerServicesRemote,
		ManagerServicesLocal {

	@PersistenceContext
	EntityManager entityManager;

	/**
	 * Default constructor.
	 */
	public ManagerServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Boolean addManager(Manager theManager) {

		Boolean b = false;
		try {
			entityManager.persist(theManager);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Manager findManagerById(Long theId) {
		return entityManager.find(Manager.class, theId);
	}

	@Override
	public Boolean updateManager(Manager theManager) {
		boolean b = false;
		try {
			entityManager.merge(theManager);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteManager(Manager theManager) {
		Boolean b = false;
		try {
			theManager = findManagerById(theManager.getYouBayUserId());
			entityManager.remove(theManager);
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@Override
	public Boolean deleteManagerById(Long theId) {
		Boolean b = false;
		try {
			entityManager.remove(findManagerById(theId));
			b = true;
		} catch (Exception e) {
		}
		return b;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Manager> findAllManager() {
		Query query = entityManager.createQuery("select e from Manager e ");
		return query.getResultList();
	}

	@Override
	public Long addManagerAndReturnId(Manager theManager) {

		try {
			entityManager.persist(theManager);
			entityManager.flush();
			return theManager.getYouBayUserId();
		} catch (Exception e) {
		}
		return null;	}

}
