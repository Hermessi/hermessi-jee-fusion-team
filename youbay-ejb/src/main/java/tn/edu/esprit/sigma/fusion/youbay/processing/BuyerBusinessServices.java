/*
 * 
 * Author Marwen
 */

package tn.edu.esprit.sigma.fusion.youbay.processing;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.BuyerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.OrderAndReviewServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.remoteservices.OrderAndReviewServicesRemote;
import tn.edu.esprit.sigma.fusion.youbay.utilities.MessagingServicesRemote;

/**
 * Session Bean implementation class BuyerBusinessServices
 */
@Stateless
@LocalBean
public class BuyerBusinessServices implements BuyerBusinessServicesRemote {
	
	@PersistenceContext
	EntityManager em;
	
	@EJB
	BuyerServicesLocal buyerServicesLocal;
	
	@EJB
	ProductServicesLocal productServicesLocal;
	
	@EJB
	OrderAndReviewServicesLocal orderAndReviewServicesLocal;
	
	@EJB
	MessagingServicesRemote messagingServicesRemote;
	
    /**
     * Default constructor. 
     */
    public BuyerBusinessServices() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void BuyProduct(Buyer buyer, Product product) {
			List<Product> products = buyer.getProducts();
			products.add(product);
			buyer.setProducts(products);
			buyerServicesLocal.updateBuyer(buyer);
	}

	@Override
	public void AddProductToCart(Buyer buyer, Product product) {
		
		List<OrderAndReview> cart = buyer.getOrderAndReviews();
		OrderAndReview productInCart = new OrderAndReview();
		orderAndReviewServicesLocal.addOrderAndReview(productInCart);
		productInCart.setBuyer(buyer);
		productInCart.setPricePaidByBuyer(product.getSellerPrice());
		productInCart.setProduct(product);
//		cart.add(productInCart);
		buyer.setOrderAndReviews(cart);
		orderAndReviewServicesLocal.addOrderAndReview(productInCart);
		buyerServicesLocal.updateBuyer(buyer);
	}
	
	@Override
	public void ConfirmOrder(Buyer buyer, OrderAndReview order) {
//		BuyProduct(buyer, order.getProduct());
		messagingServicesRemote.sendEmail(
				buyer.getEmail(),
				"Youbay subscription confirmation",
				"Thank you for purchasing "+order.getProduct().getProductName() 
		);
	}

	@Override
	public String CompareProducts(Product product1, Product product2) {
		String compareString = "";
		
		if(product1.getSellerPrice()>product2.getSellerPrice()){
			compareString = product1.getProductName() + "is Higher priced than " + product2.getProductName();
		}
		else 
		if(product1.getSellerPrice()>product2.getSellerPrice()){
			compareString = product1.getProductName() + "is Lower priced than " + product2.getProductName();
		}
		else{
			compareString = product1.getProductName() + "is Equal priced than " + product2.getProductName();
		}
		
		return compareString;
	}

	@Override
	public List<Product> FindProductsAdvanced(Float highestPrice, Float lowestPrice, Subcategory subcategory,
			String subCategoryAttributes) {
		List<Product> productsFound = productServicesLocal.findAllProduct();
		return productsFound;
	}



}
