package tn.edu.esprit.sigma.fusion.youbay.utilities;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.AssistantItems;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Auction;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Category;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ClientType;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.CustomizedAds;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.HistoryOfViews;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Manager;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.ProductHistory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.SpecialPromotion;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Subcategory;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AssistantItemsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.AuctionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.BuyerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CategoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.CustomizedAdsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.HistoryOfViewsServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ManagerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.OrderAndReviewServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductHistoryServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.ProductServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SellerServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SpecialPromotionServicesLocal;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices.SubcategoryServicesLocal;

/**
 * Session Bean implementation class InitDatabase
 */
@Stateless
@SuppressWarnings("deprecation")
public class InitDatabaseServices implements InitDatabaseServicesRemote {

	@PersistenceContext
	EntityManager em;
	@EJB
	AssistantItemsServicesLocal assistantItemsServicesLocal;
	@EJB
	AuctionServicesLocal auctionServicesLocal;
	@EJB
	BuyerServicesLocal buyerServicesLocal;
	@EJB
	CategoryServicesLocal categoryServicesLocal;
	@EJB
	CustomizedAdsServicesLocal customizedAdsServicesLocal;
	@EJB
	HistoryOfViewsServicesLocal historyOfViewsServicesLocal;
	@EJB
	ManagerServicesLocal managerServicesLocal;
	@EJB
	OrderAndReviewServicesLocal orderAndReviewServicesLocal;
	@EJB
	ProductHistoryServicesLocal productHistoryServicesLocal;
	@EJB
	ProductServicesLocal productServicesLocal;
	@EJB
	SellerServicesLocal sellerServicesLocal;
	@EJB
	SpecialPromotionServicesLocal specialPromotionServicesLocal;
	@EJB
	SubcategoryServicesLocal subcategoryServicesLocal;

	/**
	 * Default constructor.
	 * 
	 */

	public InitDatabaseServices() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void truncateAllTables() {

		int deletedCount = 0;
		// we need to keep the truncate order
		System.out
				.println("=====> BEGIN truncateAllTables BEGIN <==================");
		deletedCount = em.createQuery("DELETE FROM Auction").executeUpdate();
		System.out
				.println("------------------------------> truncateAllTables : deleted "
						+ deletedCount + " Auction <-------");

		deletedCount = em.createQuery("DELETE FROM AssistantItems")
				.executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " AssistantItems <-------");

		// ATTENTION : Search for a solution to reset AUTO_INCREMENT
		// em.createNativeQuery("ALTER TABLE t_assistantitems AUTO_INCREMENT = 1").executeUpdate();

		deletedCount = em.createQuery("DELETE FROM SpecialPromotion")
				.executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " SpecialPromotion <-------");
		deletedCount = em.createQuery("DELETE FROM ProductHistory")
				.executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " ProductHistory <-------");

		deletedCount = em.createQuery("DELETE FROM CustomizedAds")
				.executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " CustomizedAds <-------");

		deletedCount = em.createQuery("DELETE FROM HistoryOfViews")
				.executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " HistoryOfViews <-------");

		deletedCount = em.createQuery("DELETE FROM OrderAndReview")
				.executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " OrderAndReview <-------");


		deletedCount = em.createQuery("DELETE FROM Category").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " Sellers <-------");

		deletedCount = em.createQuery("DELETE FROM Product").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " Product <-------");

		deletedCount = em.createQuery("DELETE FROM Subcategory")
				.executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " Subcategory <-------");

		System.out
				.println("==================> END truncateAllTables END <==================");
		
		// we need to keep the truncate order
		deletedCount = em.createQuery("DELETE FROM Manager").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " Managers <-------");

		deletedCount = em.createQuery("DELETE FROM Buyer").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " Buyers <-------");

		deletedCount = em.createQuery("DELETE FROM Seller").executeUpdate();
		System.out.println("-----> truncateAllTables : deleted " + deletedCount
				+ " Sellers <-------");
		

	}

	@Override
	public void addUsers() {
		/*
		 * Adds 9 USER 3 manager + 3 seller + 3 buyer
		 */
		System.out.println("-----> Adding Users <-----");

		Buyer buyera1 = new Buyer("Houssem", "Sabbegh", "sabbegh@gmail.com",
				"21623215434", new Date("17/05/1991"), "tunisia", true, false,
				"", "1 Purchase;", true, 0f, "18 rue ESPRIT", "", "Ghazela",
				0f, 15f);

		Buyer buyera2 = new Buyer("Yassine", "Latiri", "pro@latiri.com",
				"21623165189", new Date("17/05/1991"), "tunisia", true, false,
				"", "5 Purchases;", true, 0f, "18 rue ESPRIT", "", "Ghazela",
				0f, 15f);

		buyerServicesLocal.addBuyer(buyera1);
		buyerServicesLocal.addBuyer(buyera2);

		Seller seller1 = new Seller("seller1 first name", "seller1 last name",
				"seller1@seller.com", "+21623256434", new Date(2015, 05, 18),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f,
				"the porpuse of the seller is to test ", 10f, false,
				"logo string");

		Seller seller2 = new Seller("seller2 first name", "seller2 last name",
				"seller2@seller.com", "+216238434", new Date(2015, 05, 18),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f,
				"the porpuse of the seller2 is to test ", 10f, false,
				"logo string");

		Seller seller3 = new Seller("seller3 first name", "seller3 last name",
				"seller3@seller.com", "+2162815434", new Date(2015, 05, 17),
				"TUNISIA", true, false, "", "emptybadges", 1000f, 10f,
				"the porpuse of the seller is to test ", 10f, false,
				"logo string");

		Manager manager1 = new Manager("manager1 first name",
				"manager1 last name", "manager1@manager.com", "+216238785434",
				new Date(2015, 05, 18), "Tunisia", true, false, "", true, true,
				true, true, true);

		Manager manager2 = new Manager("manager2 first name",
				"manager2 last name", "manager2@manager.com", "+21627434",
				new Date(2015, 05, 18), "Tunisia", true, false, "", true, true,
				true, true, true);
		Manager manager3 = new Manager("manager3 first name",
				"manager3 last name", "manager3@manager.com",
				"+21623884215434", new Date(2015, 05, 18), "Tunisia", true,
				false, "", true, true, true, true, true);

		Buyer buyer1 = new Buyer("buyer1 first name", "buyer 1 last name",
				"buyer1@buyer.com", "+216232134", new Date(2015 / 05 / 17),
				"TUNISIA", true, true, "", "no budget", true, 1547f, "lac II",
				"canada embacy", "tunisia", 54847954f, 41f);
		Buyer buyer2 = new Buyer("buyer2 first name", "buyer 2 last name",
				"buyer2@buyer.com", "+21625434", new Date(2015 / 05 / 17),
				"TUNISIA", true, true, "", "no budget", true, 1547f, "lac II",
				"canada embacy", "tunisia", 54847954f, 41f);
		Buyer buyer3 = new Buyer("buyer3 first name", "buyer 3 last name",
				"buyer3@buyer.com", "+21623215", new Date(2015 / 05 / 17),
				"TUNISIA", true, true, "", "no budget", true, 1547f, "lac II",
				"canada embacy", "tunisia", 54847954f, 41f);

		sellerServicesLocal.addSeller(seller1);
		sellerServicesLocal.addSeller(seller2);
		sellerServicesLocal.addSeller(seller3);
		buyerServicesLocal.addBuyer(buyer1);
		buyerServicesLocal.addBuyer(buyer2);
		buyerServicesLocal.addBuyer(buyer3);
		managerServicesLocal.addManager(manager1);
		managerServicesLocal.addManager(manager2);
		managerServicesLocal.addManager(manager3);
	}

	@Override
	public void addAssistantItems() {
		AssistantItems assistantItems1 = new AssistantItems(10,
				"assistantItems1quetion1", "assistantItems1negativeanswer1",
				"assistantItems1affirmativeanswer1",
				"negativeAnswerQueryassistantItems1",
				"affirmativeAnswerQueryassistantItems1");

		AssistantItems assistantItems2 = new AssistantItems(10,
				"assistantItems2quetion1", "assistantItems2negativeanswer1",
				"assistantItems2affirmativeanswer1",
				"negativeAnswerQueryassistantItems2",
				"affirmativeAnswerQueryassistantItems2");

		AssistantItems assistantItems3 = new AssistantItems(10,
				"assistantItems3quetion1", "assistantItems3negativeanswer1",
				"assistantItems3affirmativeanswer1",
				"negativeAnswerQueryassistantItems3",
				"affirmativeAnswerQueryassistantItems3");

		assistantItemsServicesLocal.addAssistantItems(assistantItems1);
		assistantItemsServicesLocal.addAssistantItems(assistantItems2);
		assistantItemsServicesLocal.addAssistantItems(assistantItems3);

	}

	@Override
	public void addAuction() {
		Auction auction1 = new Auction(new Date(2015, 17, 05), new Date(2015,
				17, 06), 10f);
		Auction auction2 = new Auction(new Date(2015, 17, 05), new Date(2015,
				17, 06), 10f);
		Auction auction3 = new Auction(new Date(2015, 17, 05), new Date(2015,
				17, 06), 10f);
		auctionServicesLocal.addAuction(auction1);
		auctionServicesLocal.addAuction(auction2);
		auctionServicesLocal.addAuction(auction3);
	}

	@Override
	public void addCategory() {
		Category category1 = new Category("Category1", 10);
		Category category2 = new Category("Category2", 20);
		Category category3 = new Category("Category3", 30);

		categoryServicesLocal.addCategory(category1);
		categoryServicesLocal.addCategory(category2);
		categoryServicesLocal.addCategory(category3);
	}

	@Override
	public void addCustomizedAds() {
		CustomizedAds customizedAds1 = new CustomizedAds(
				new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 1 message", true, true);
		CustomizedAds customizedAds2 = new CustomizedAds(
				new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 2 message", true, true);
		CustomizedAds customizedAds3 = new CustomizedAds(
				new Date(1991, 05, 17), new Date(2015, 05, 18), 254f,
				"customized ads 3 message", true, true);

		customizedAdsServicesLocal.addCustomizedAds(customizedAds1);
		customizedAdsServicesLocal.addCustomizedAds(customizedAds2);
		customizedAdsServicesLocal.addCustomizedAds(customizedAds3);

	}

	@Override
	public void addProduct() {

		Product producta1 = new Product("Acer Laptop", "", "Acer Laptop",
				1000f, "", false, false, 5);

		Product producta2 = new Product("HP Laptop", "", "HP Laptop", 1000f,
				"", false, false, 5);

		Product producta3 = new Product("Toshiba Laptop", "", "Toshiba Laptop",
				1000f, "", false, false, 5);

		productServicesLocal.addProduct(producta1);
		productServicesLocal.addProduct(producta2);
		productServicesLocal.addProduct(producta3);

		Product product1 = new Product("product1 name", "product 1 image url",
				"product 1 description", 10f, "product1 Description", true,
				true, 10);
		Product product2 = new Product("product1 name", "product 1 image url",
				"product 1 description", 10f, "product1 Description", true,
				true, 10);
		Product product3 = new Product("product1 name", "product 1 image url",
				"product 1 description", 10f, "product1 Description", true,
				true, 10);
		productServicesLocal.addProduct(product1);
		productServicesLocal.addProduct(product2);
		productServicesLocal.addProduct(product3);

	}

	@Override
	public void addProductHistory() {
		ProductHistory productHistory1 = new ProductHistory(new Date(1478, 05,
				17), "product history1 name", "product history1 image URL",
				"short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		ProductHistory productHistory2 = new ProductHistory(new Date(1478, 05,
				17), "product history2 name", "product history2 image URL",
				"short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		ProductHistory productHistory3 = new ProductHistory(new Date(1478, 05,
				17), "product history3 name", "product history3 image URL",
				"short description", "productMainDescriptionHistory", 10f,
				"subcategoryAdditionalValuesHistory", 10);
		productHistoryServicesLocal.addProductHistory(productHistory1);
		productHistoryServicesLocal.addProductHistory(productHistory2);
		productHistoryServicesLocal.addProductHistory(productHistory3);

	}

	@Override
	public void addSpecialPromotion() {
		SpecialPromotion promotion1 = new SpecialPromotion(new Date(2014, 05,
				17), new Date(2014, 05, 17), false, "deal1 ShortDescription",
				"deal1 Description", 5f);
		SpecialPromotion promotion2 = new SpecialPromotion(new Date(2014, 05,
				17), new Date(2014, 05, 17), false, "deal2 ShortDescription",
				"deal2 Description", 5f);
		SpecialPromotion promotion3 = new SpecialPromotion(new Date(2014, 05,
				17), new Date(2014, 05, 17), false, "deal3 ShortDescription",
				"deal3 Description", 5f);
		specialPromotionServicesLocal.addSpecialPromotion(promotion1);
		specialPromotionServicesLocal.addSpecialPromotion(promotion2);
		specialPromotionServicesLocal.addSpecialPromotion(promotion3);

	}

	@Override
	public void addSubcategory() {
		Subcategory subcategory1 = new Subcategory("categorie1 name", 10,
				"subcategoryAttributesAndTypes", true, "avatar image");
		Subcategory subcategory2 = new Subcategory("categorie2 name", 10,
				"subcategoryAttributesAndTypes", true, "avatar image");
		Subcategory subcategory3 = new Subcategory("categorie3 name", 10,
				"subcategoryAttributesAndTypes", true, "avatar image");
		subcategoryServicesLocal.addSubcategory(subcategory1);
		subcategoryServicesLocal.addSubcategory(subcategory2);
		subcategoryServicesLocal.addSubcategory(subcategory3);
	}

	@Override
	public void addHistoryOfViews() {
		/* Always run this method AFTER running addUsers and addProdutcs ! */

		 List<Buyer> listBuyers = buyerServicesLocal.findAllBuyer();
		 List<Product> listProducts = productServicesLocal.findAllProduct();
		
		 HistoryOfViews historyItem1 = new
		 HistoryOfViews("Added automatically",
		 ClientType.other, listBuyers.get(0), listProducts.get(0),
		 new Date());
		 HistoryOfViews historyItem2 = new
		 HistoryOfViews("Added automatically",
		 ClientType.other, listBuyers.get(0), listProducts.get(1),
		 new Date());
		 HistoryOfViews historyItem3 = new
		 HistoryOfViews("Added automatically",
		 ClientType.other, listBuyers.get(1), listProducts.get(0),
		 new Date());
		
		 historyOfViewsServicesLocal.addHistoryOfViews(historyItem1);
		 historyOfViewsServicesLocal.addHistoryOfViews(historyItem2);
		 historyOfViewsServicesLocal.addHistoryOfViews(historyItem3);

	}

	@Override
	public void addOrderAndReview() {
		/* Always run this method AFTER running addUsers and addProdutcs ! */

		 List<Buyer> listBuyers = buyerServicesLocal.findAllBuyer();
		 List<Product> listProducts = productServicesLocal.findAllProduct();
		
		 OrderAndReview order1 = new OrderAndReview(15f,
		 "Please package the items carefuly !", false, false,
		 new Date(), false, false, null, null, null, listBuyers.get(0),
		 listProducts.get(0));
		
		 OrderAndReview order2 = new OrderAndReview(15f,
		 "Please package the items carefuly !", false, false,
		 new Date(), false, false, null, null, null, listBuyers.get(1),
		 listProducts.get(1));
		
		 orderAndReviewServicesLocal.addOrderAndReview(order1);
		 orderAndReviewServicesLocal.addOrderAndReview(order2);
	}

	@Override
	public void populateDatabase() {

		/* Always run these methods FIRST */
		addUsers();
		addAssistantItems();
		addAuction();
		addCategory();
		addCustomizedAds();
		addProduct();
		addProductHistory();
		addSpecialPromotion();
		addSubcategory();

		/* Always run these methods LAST */
		addHistoryOfViews();
		addOrderAndReview();

	}

}
