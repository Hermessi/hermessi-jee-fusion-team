package tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.localservices;

import java.util.List;

import javax.ejb.Local;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReview;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.OrderAndReviewId;

@Local
public interface OrderAndReviewServicesLocal {

	Boolean addOrderAndReview(OrderAndReview theOrderAndReview);

	OrderAndReview findOrderAndReviewById(OrderAndReviewId theOrderAndReviewIdClass);
	
	Boolean updateOrderAndReview(OrderAndReview theOrderAndReview);

	Boolean deleteOrderAndReviewById(OrderAndReviewId theOrderAndReviewIdClass);

	List<OrderAndReview> findAllOrderAndReview();
}
