package tn.edu.esprit.sigma.fusion.gui;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.RowFilter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;

public class ListofBuyers {

	public JFrame frame;
	private JTable table;
	
	private ListBuyersModel listBuyers;
	public JTextField txtFind;
	private JLabel lblFindSellers;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListofBuyers window = new ListofBuyers();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ListofBuyers() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 630, 403);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		txtFind = new JTextField();
		txtFind.setColumns(10);
		
		
		
		
		listBuyers = new ListBuyersModel();
		table = new JTable(listBuyers);
		
		
		final TableRowSorter<TableModel> rowSorter = new TableRowSorter<>(table.getModel());
		
		table.setRowSorter(rowSorter);
		
		txtFind.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				String text = txtFind.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				String text = txtFind.getText();
				if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
				
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				throw new UnsupportedOperationException("Not supported yet.");
				
			}
		});
		
		
		JButton btnSendSms = new JButton("Send SMS");
		btnSendSms.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedRow = table.getSelectedRow();
				if(selectedRow > -1){
					SMS smsFrame = new SMS();
					frame.setVisible(false);
					smsFrame.frame.setVisible(true);
					smsFrame.textField.setText((String)table.getValueAt(selectedRow, 4).toString());
				}
				
			}
		});
		
		JLabel lblListOfSellers = new JLabel("List of Sellers");
		lblListOfSellers.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		lblFindSellers = new JLabel("Find Sellers");
		lblFindSellers.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(469, Short.MAX_VALUE)
					.addComponent(btnSendSms)
					.addGap(66))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(78)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblListOfSellers)
						.addComponent(table, GroupLayout.PREFERRED_SIZE, 479, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblFindSellers)
							.addGap(46)
							.addComponent(txtFind, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(57, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(51)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblFindSellers)
						.addComponent(txtFind, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
					.addComponent(lblListOfSellers)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
					.addGap(42)
					.addComponent(btnSendSms)
					.addGap(24))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
