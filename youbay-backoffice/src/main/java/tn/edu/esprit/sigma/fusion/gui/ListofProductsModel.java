package tn.edu.esprit.sigma.fusion.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Product;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;

public class ListofProductsModel extends AbstractTableModel {
	
	List<Product> listeProducts = new ArrayList<Product>();
	private final String[] entetes = { "Id", "First Name", "Last Name", "Email", "Phone" };
	
	public ListofProductsModel(){
		listeProducts = BasicFonctionnalitiesDelegate.doFindAllProduct();
    }

	@Override
	public int getRowCount() {
		return listeProducts.size();
	}

	@Override
	public int getColumnCount() {
		return entetes.length;
	}

	@Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
	        case 0:
	            return listeProducts.get(rowIndex).getProductId();
            case 1:
                return listeProducts.get(rowIndex).getProductName();
            case 2:
                return listeProducts.get(rowIndex).getQuantityAvailable();
            case 3:
                return listeProducts.get(rowIndex).getSubcategory();   
            case 4:
                return listeProducts.get(rowIndex).getQuantityAvailable();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) { // nom des colonnes
        return entetes[column]; 
    }

}
