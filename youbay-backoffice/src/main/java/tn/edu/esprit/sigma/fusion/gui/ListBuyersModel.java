package tn.edu.esprit.sigma.fusion.gui;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Buyer;
import tn.edu.esprit.sigma.fusion.youbay.basicfonctionnalities.entities.Seller;
import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;

public class ListBuyersModel extends AbstractTableModel {
	
	List<Seller> listeBuyers = new ArrayList<Seller>();
	private final String[] entetes = { "Id", "First Name", "Last Name", "Email", "Phone" };
	
	public ListBuyersModel(){
		listeBuyers = BasicFonctionnalitiesDelegate.doFindAllSeller();
    }

	@Override
	public int getRowCount() {
		return listeBuyers.size();
	}

	@Override
	public int getColumnCount() {
		return entetes.length;
	}

	@Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
	        case 0:
	            return listeBuyers.get(rowIndex).getYouBayUserId();
            case 1:
                return listeBuyers.get(rowIndex).getFirstName();
            case 2:
                return listeBuyers.get(rowIndex).getLastName();
            case 3:
                return listeBuyers.get(rowIndex).getEmail();   
            case 4:
                return listeBuyers.get(rowIndex).getPhoneNumber();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int column) { // nom des colonnes
        return entetes[column]; 
    }

}
