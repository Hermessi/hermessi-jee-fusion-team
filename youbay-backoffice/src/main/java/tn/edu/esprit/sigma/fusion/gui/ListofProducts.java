package tn.edu.esprit.sigma.fusion.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.RowFilter;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JButton;




import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.SMSDelegate;


import tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate.BasicFonctionnalitiesDelegate;
import tn.edu.esprit.sigma.fusion.gui.ListofBuyers;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;

public class ListofProducts {

	private JFrame Pframe;
	private JTextField textFind;
	private JTable table;
	
	private ListofProductsModel listeProducts;
	private JLabel lblFindProduct;
	private JLabel lblListOfProducts;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListofProducts window = new ListofProducts();
					window.Pframe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ListofProducts() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Pframe = new JFrame();
		Pframe.setBounds(100, 100, 611, 392);
		Pframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		textFind = new JTextField();
		textFind.setColumns(10);
		
		listeProducts = new ListofProductsModel();
		table = new JTable(listeProducts);
		
final TableRowSorter<TableModel> rowSorter = new TableRowSorter<>(table.getModel());
		
		table.setRowSorter(rowSorter);
		
		textFind.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				String text = textFind.getText();

                if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				String text = textFind.getText();
				if (text.trim().length() == 0) {
                    rowSorter.setRowFilter(null);
                } else {
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
				
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				throw new UnsupportedOperationException("Not supported yet.");
				
			}
		});
		
		
		JButton btnDeleat = new JButton("Deleat");
		btnDeleat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//String msg =textField_1.getText();
				int selectedRow = table.getSelectedRow();
				
				
				if(selectedRow > -1){
					BasicFonctionnalitiesDelegate.doDeleteProductById((Long)table.getValueAt(selectedRow,0));
					ListofBuyers Frame = new ListofBuyers();
					Pframe.setVisible(false);
					Frame.frame.setVisible(true);
					Frame.txtFind.setText((String)table.getValueAt(selectedRow, 4).toString());
				}
				
				
			}
		});
		
		lblFindProduct = new JLabel("Find Product");
		lblFindProduct.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		lblListOfProducts = new JLabel("List of Products");
		lblListOfProducts.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GroupLayout groupLayout = new GroupLayout(Pframe.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(449, Short.MAX_VALUE)
					.addComponent(btnDeleat)
					.addGap(83))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(97)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblListOfProducts)
						.addComponent(table, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblFindProduct)
							.addGap(55)
							.addComponent(textFind, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(143))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(48)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblFindProduct)
						.addComponent(textFind, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(28)
					.addComponent(lblListOfProducts)
					.addGap(18)
					.addComponent(table, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
					.addComponent(btnDeleat)
					.addGap(27))
		);
		Pframe.getContentPane().setLayout(groupLayout);
	}

}
