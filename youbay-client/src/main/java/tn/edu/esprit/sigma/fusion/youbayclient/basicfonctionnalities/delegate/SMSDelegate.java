package tn.edu.esprit.sigma.fusion.youbayclient.basicfonctionnalities.delegate;

import tn.edu.esprit.sigma.fusion.youbay.utilities.SMSServiceRemote;
import tn.edu.esprit.sigma.fusion.youbayclient.locator.ServiceLocator;

public class SMSDelegate {

	public static final String jndiName = "youbay-ejb/SMSService!tn.edu.esprit.sigma.fusion.youbay.utilities.SMSServiceRemote";

	public static SMSServiceRemote getProxy() {
		return (SMSServiceRemote) ServiceLocator.getInstance().getProxy(
				jndiName);
	}
	
	public static void doSendSMS(String num, String msg) {
		getProxy().sendSMS( num , msg);
	}

}
